# STEP 1 build executable binary
FROM percona

COPY *.sql /docker-entrypoint-initdb.d

ENV MYSQL_ROOT_PASSWORD=admin

EXPOSE 3306
