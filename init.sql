CREATE DATABASE `grocery-project` COLLATE 'utf8_unicode_ci';;
DROP TABLE IF EXISTS `grocery-project`.`products`;
DROP TABLE IF EXISTS `grocery-project`.`users`;

CREATE TABLE `grocery-project`.`products` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `label` varchar(50) NOT NULL,
  `price` float NOT NULL,
  `image_url` varchar(300) NULL
);

CREATE TABLE `grocery-project`.`authentification_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `value` varchar(200) NOT NULL UNIQUE,
  `expiration_date` datetime NOT NULL,
  `status` varchar(20) NOT NULL
);

CREATE TABLE `grocery-project`.`users` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `user_password` varchar(50) NOT NULL,
  `birth_date` date NOT NULL,
  `email` varchar(100) NOT NULL UNIQUE,
  `phone_number` varchar(10) NOT NULL UNIQUE,
  `authentification_token_id` int(11) NOT NULL UNIQUE,
  FOREIGN KEY (authentification_token_id) REFERENCES authentification_token(id)
);

CREATE EVENT `grocery-project`.`check_jwt_expiration_date`
ON SCHEDULE EVERY '1' DAY STARTS CURRENT_TIMESTAMP ON COMPLETION PRESERVE
ENABLE COMMENT '' DO
UPDATE authentification_token SET status='EXPIRED' WHERE NOW() >= expiration_date;

SET GLOBAL event_scheduler = 1 ;

INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Ail', '8.91', 'https://entrainement-sportif.fr/ail-1.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Courgette', '3', 'https://www.lesfruitsetlegumesfrais.com/_upload/cache/ressources/produits/courgette/courgette_346_346_filled.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Aneth', '2.70', 'https://cuisinealouest.com/wp-content/uploads/2017/07/aneth-1200x798.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Echalotes', '1.35', 'https://o-pres.fr/wp-content/uploads/2018/01/10663185.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Asperge', '2.39', 'https://static.passeportsante.net/i58398-asperge.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Fèves', '4.60', 'https://www.lesfruitsetlegumesfrais.com/_upload/cache/ressources/produits/feve/feve_346_346_filled.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Aubergine', '2.40', 'https://cac.img.pmdstatic.net/fit/http.3A.2F.2Fprd2-bone-image.2Es3-website-eu-west-1.2Eamazonaws.2Ecom.2FCAC.2F2018.2F09.2F25.2Fc2e6d264-44d1-422e-b453-7962d334f4a1.2Ejpeg/750x562/quality/80/crop-from/center/aubergine.jpeg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Haricots', '0.95', 'https://cdn-europe1.lanmedia.fr/var/europe1/storage/images/europe1/faits-divers/une-grenouille-morte-dans-une-conserve-de-haricots-verts-970282/19028626-1-fre-FR/Une-grenouille-morte-dans-une-conserve-de-haricots-verts.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Betterave', '1.45', 'https://img-3.journaldesfemmes.fr/gWjzywLzmQyqsPz5nQMEhC7dFOQ=/910x607/smart/cef55d2688b34f18882bacfed76113fd/ccmcms-jdf/10659068.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Laitue', '0.95', 'https://img-3.journaldesfemmes.fr/Hwbeol52Jz1DRBwgjBjrkmDF4Zg=/910x607/smart/e768c994317848edb31ced2ab6a2f35e/ccmcms-jdf/10661612.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Brocoli', '2.40', 'https://www.lesfruitsetlegumesfrais.com/_upload/cache/ressources/produits/brocolis/brocoli_346_346_filled.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Champignons', '2.50', 'https://st.depositphotos.com/1000135/3890/i/950/depositphotos_38904395-stock-photo-champignon-mushrooms.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Poireau', '1.06', 'https://img-3.journaldesfemmes.fr/5RXyJNI0QPmtcH-Kuf8SpfpotwQ=/910x607/smart/41fc5ac5817947d29ba18067c1b7974b/ccmcms-jdf/10660346.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Chou', '0.97', 'https://static.greenweez.com/images/products/87000/600/les-paysans-bio-chou-vert-lisse-bio-1-piece.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Poivron', '1.96', 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/de/Capsicum_annuum_fruits_IMGP0049.jpg/290px-Capsicum_annuum_fruits_IMGP0049.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Chou-fleur', '1.74', 'https://image.afcdn.com/recipe/20170607/67526_w300h300c1.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Pomme de terre', '2.99', 'https://www.comptoirdesjardins.fr/2058-thickbox_default/plants-de-pommes-de-terre-bio-passion-3-kg.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Ciboulette', '1.85', 'https://www.diamantvoyance.fr/wp-content/uploads/2017/04/ciboulette.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Radis', '1.30', 'https://img-3.journaldesfemmes.fr/HpQSq5FhrpiwCkSulffBZdTAN10=/910x607/smart/20f13ddfde67407c827768eaba7948a2/ccmcms-jdf/10660880.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Concombre', '0.99', 'https://www.lesfruitsetlegumesfrais.com/_upload/cache/ressources/produits/concombre/concombre_346_346_filled.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Tomates', '1.25', 'https://www.auchandirect.fr/backend/media/products_images/0N_57387.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Raisins verts', '1.09', 'https://previews.123rf.com/images/yurakp/yurakp1505/yurakp150500297/40010140-raisin-vert-isol%C3%A9-sur-le-fond-blanc-.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Avocat', '2.60', 'https://www.regal.fr/sites/art-de-vivre/files/styles/large/public/avocat_th.jpg?itok=fXNtrXFF');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Mandarine', '1.50', 'https://img-3.journaldesfemmes.fr/Kzj8RKSWi6rqF9RJ3exWH8UB-hE=/910x607/smart/2999942f22784358bbcecbd204d89fb3/ccmcms-jdf/10660242.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Banane', '2.30', 'https://www.auchandirect.fr/backend/media/products_images/0N_57300.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Mangue', '0.99', 'https://lel.s3.amazonaws.com/assets/images/recettes/carpaccio_de_mangue_%C3%A0_la_cannelle.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Melon', '3', 'https://cdn3.couleurs-paysannes.fr/961-tm_large_default/melon-charentais.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Cerise', '2.70', 'https://cac.img.pmdstatic.net/fit/http.3A.2F.2Fprd2-bone-image.2Es3-website-eu-west-1.2Eamazonaws.2Ecom.2FCAC.2F2018.2F09.2F25.2Fe3eefc02-cbf7-469f-abcf-1130ef4faa06.2Ejpeg/750x562/quality/80/crop-from/center/cerise.jpeg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Netarine', '1.35', 'https://www.jardindorante.fr/112-large_default/mai-2012-nectarine.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Citron', '2.39', 'https://cac.img.pmdstatic.net/fit/http.3A.2F.2Fprd2-bone-image.2Es3-website-eu-west-1.2Eamazonaws.2Ecom.2FCAC.2F2018.2F09.2F25.2Faf7c4dcb-2050-4fbc-ad9e-ed8ff68f451f.2Ejpeg/750x562/quality/80/crop-from/center/citron.jpeg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Orange', '4.60', 'https://www.lesfruitsetlegumesfrais.com/_upload/cache/ressources/produits/orange/orange_346_346_filled.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Clémentine', '2.40', 'https://img-3.journaldesfemmes.fr/TxldyNVJjFtgPopx9CE8TcLM8kQ=/1280x/smart/192beaf755694b24950ec5e18b5a0add/ccmcms-jdf/10661559.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Pamplemousse', '0.95', 'https://staging.andros.ch/admin/wp-content/uploads/2018/03/pamplemousse.png');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Dattes', '1.45', 'https://www.vulgaris-medical.com/sites/default/files/styles/big-lightbox/public/field/image/actualites/2015/12/30/les-bienfaits-de-la-datte.jpg?itok=6MBoPXsT');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Pêche', '0.95', 'https://www.cathyprimeurs.com/images/Image/Peche.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Fraises', '2.40', 'https://www.corsicavap.com/727-thickbox_default/fraises-fin-de-production.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Poire', '2.50', 'https://image.afcdn.com/recipe/20170607/67498_w300h300c1cx350cy350.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Framboises', '1.06', 'https://img-3.journaldesfemmes.fr/lZqm2rIC2PMKPP1zVrNP7uFsyoo=/910x607/smart/91eed6d1ec5c4912849bc192c62161ab/ccmcms-jdf/10662426.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Pomme', '0.97', 'https://media.gerbeaud.net/2017/01/640/pomme-detouree.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Pruneau', '1.96', 'https://www.gourmandiseries.fr/wp-content/uploads/2018/09/pruneau-maison.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Raisins bleus', '1.74', 'https://www.monpanierbio.com/wp-content/uploads/2018/04/raisin-bleu-local.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Raisins rouges', '2.99', 'https://i5.walmartimages.ca/images/Large/098/5_r/6000195440985_R.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Kiwi', '1.85', 'https://image.afcdn.com/recipe/20170607/67631_w300h300c1cx350cy350.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Raisins verts', '1.30', 'https://previews.123rf.com/images/utima/utima1512/utima151200036/50417871-raisins-verts-%C3%A0-feuilles-isol%C3%A9-sur-blanc.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Bagels', '0.99', 'https://img.buzzfeed.com/thumbnailer-prod-us-east-1/16f7a80a111f4560ab217d1ba2d1730e/BFV36537_CC2017_2IngredintDough4Ways-FB.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Muffins anglais', '1.25', 'https://recettehealthy.com/wordpress/wp-content/uploads/2016/10/muffin-anglais-1300x975.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Pain baguette', '1.09', 'https://static.lexpress.fr/medias_11483/w_2048,h_1146,c_crop,x_0,y_0/w_480,h_270,c_fill,g_north/v1494863617/baguette-pain-boulangerie_5879711.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Brioches', '2.60', 'https://www.moulinex.fr/medias/?context=bWFzdGVyfHJvb3R8NjE4NTJ8aW1hZ2UvanBlZ3xoOTAvaGY5LzEzMTAxNjUwNDQ0MzE4LmpwZ3w4NTgzMDY0ZWI5Mjg4OGZlNGMzZjViZjdjYWMwZTcxYjk2Zjc1MzI1Y2Y2MjZhYmI1NGUyYjMyNGY5MDhkZDVj');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Pain tranché', '1.50', 'https://static.lecomptoirlocal.fr/img/produits/ef34d2018957879fc96cfe8583dbf2e1/large.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Croissants', '2.30', 'https://1.bp.blogspot.com/-iIYn5xbAfec/Uv9z2PJBsHI/AAAAAAAADgg/KyVCfloMvaY/s1600/croissants-parisiens.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Pain hot-dog', '0.99', 'https://www.hotdog-town.com/369-large_default/4-petits-pains-brioches-a-hot-dogs.jpg');
INSERT INTO `grocery-project`.`products` (`label`, `price`, `image_url`) VALUES ('Pain hamburger', '2.70', 'https://www.cookomix.com/wp-content/uploads/2016/08/pains-hamburger-thermomix-800x600.jpg');

INSERT INTO `grocery-project`.`authentification_token` (`id`, `value`, `expiration_date`, `status`) VALUES (1, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImpvaG4uc25vd0Bob3RtYWlsLmZyIiwicGFzc3dvcmQiOiJteXBhc3N3b3JkIn0.c1buy0v0Q-Ff3i6CKWheilusTkgmR-lJH8xjH66lfi4', '9999-12-31 23:59:59', "ACTIF");

INSERT INTO `grocery-project`.`users` (`firstname`, `lastname`, `user_password`, `birth_date`, `email`, `phone_number`, `authentification_token_id`) VALUES ('John', 'Snow', 'mypassword', '1985-04-18', 'john.snow@hotmail.fr', '0665635880', 1);
